use strict;
use warnings;
use utf8;
use Test::More;
use FizzBuzzProgram;
binmode(STDOUT, ":utf8");
binmode Test::More->builder->$_, ":utf8" for qw/output failure_output todo_output/;                       
no warnings 'redefine';
my $code = \&Test::Builder::child;
*Test::Builder::child = sub {
    my $builder = $code->(@_);
    binmode $builder->output,         ":utf8";
    binmode $builder->failure_output, ":utf8";
    binmode $builder->todo_output,    ":utf8";
    return $builder;
};

subtest '入力値チェック' => sub {
    subtest '正常値チェック' => sub {
        subtest 'FizzBuzzProgramのオブジェクトを作成' => sub{
            my $obj = FizzBuzzProgram->new();
            isa_ok $obj, 'FizzBuzzProgram';

            subtest '1が入力された時' => sub{
                my $expect = $obj->check_mode(1);
                is($expect,1);
            };
            subtest '0が入力された時' => sub{
                my $expect = $obj->check_mode(0);
                is($expect,0);
            };
        };
    };
};

done_testing;

