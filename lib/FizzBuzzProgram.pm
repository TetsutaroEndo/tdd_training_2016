package FizzBuzzProgram;
use strict;
use warnings;
use utf8;

sub new {
    my $class = shift;
    my $self = {
    @_,
    };

    return bless $self, $class;
}

sub check_mode {
  my ($class, $mode) = @_;

  if($mode == 0) {
    print 'Exit Program.';
    return 0;
  }
  if($mode==1){
    return 1;
  }
}

sub user_input{
    print 'メニューを選択してください。';
    my $mode = <STDIN>;
    return $mode;
}

1;
